from pathlib import Path

import kivalu
import pytest


@pytest.fixture(scope="module")
def server():
    with kivalu.TestServer() as server:
        yield server


@pytest.fixture()
def client():
    return kivalu.Client(url="http://localhost:8000", configuration_file=Path(""))
