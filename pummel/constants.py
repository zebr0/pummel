from pathlib import Path

ENCODING = "utf-8"

REPORTS_PATH_DEFAULT = Path("/var/pummel")
KEY_DEFAULT = "script"
ATTEMPTS_DEFAULT = 4
PAUSE_DEFAULT = 10
VARIANT_DEFAULT = ""

INCLUDE = "include"
COMMAND = "command"
VARIANT = "variant"
KEY = "key"
TARGET = "target"
CONTENT = "content"
STATUS = "status"
